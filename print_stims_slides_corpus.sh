#!/bin/sh
# Time-stamp: <2011-12-16 11:55:32 amoebe>

# Randomize
#sed 1d 20111215-1-kiy-ap-framedwordlist.txt > temp
#perl ~/docs/mind/scripts/perl/rand_lines.pl temp > 20111215-1-kiy-ap-framedwordlist-rand.txt
#rm -f temp
sed 1d 20111216-4-kiy-ap-corpus.txt > temp
perl ~/docs/mind/scripts/perl/rand_lines.pl temp > 20111216-4-kiy-ap-corpus-rand.txt
rm -f temp

# Print tex file
perl print_stims_slides_corpus.pl 20111216-4-kiy-ap-corpus-rand.txt > 20111216-4-kiy-ap-corpus.tex

# Output tex file as pdf
pdflatex 20111216-4-kiy-ap-corpus.tex